# building the container
#                         container_name:tag
# ```podman build . -t schedyl_container:1```

# running
    # getting ID: ```podman images```
# ```podman run -it IMAGE_ID /bin/bash```

# if it is already built, then execute
# ```podman start container_id
#    podman attach container_id```

#FROM gcc:latest
FROM ubuntu:latest

# install dependencies
RUN apt-get update && DEBIAN_FRONTEND=noninteractive apt-get install -y build-essential git unzip wget cmake default-jdk maven
RUN echo "JAVA_HOME=\"/usr/lib/jvm/java-11-openjdk-amd64\"" >> /etc/environment
# "." is something like source
RUN . /etc/environment

# install Schedyl dependencies (i.e. libraries that it uses)
WORKDIR /usr/src/schedyl/

ADD install.sh .

# install dependencies in vendor/ folder
RUN chmod +x install.sh
RUN ./install.sh
