# Schedyl

 [![pipeline status](https://gitlab.com/sdvorak/schedyl/badges/main/pipeline.svg)](https://gitlab.com/sdvorak/schedyl/-/commits/main)

*Schedyl* might be helpful utility for students creating optimal schedules and maybe more...

[[_TOC_]]

## Features
- exactly one *scheduling event* of *type* *"lecture"* and *"lab"*, for each *scheduling subjects*,
- all the selected *scheduling events* are day+time disjoint,
- empty day constraint,
- finding schedule with minimal cost; current cost function implements these features:
  - minimize **time-gaps** between adjacent selected scheduling events,
  - minimize **transfers** between buildings.


# User Documentation

## Usage
```
./Schedyl_main [action] [action parameters]
```

### Action & action parameters
- find
  - \[file with scheduling events\].json (\[criteria\].json)? \[temporary cnf file\].cnf
- plugin_sis_transform
  - \[file.html\]

## Installation

```
git clone https://gitlab.com/sdvorak/schedyl.git
cd schedyl
./install.sh
cmake -Bcmake-build-debug/ -DCMAKE_BUILD_TYPE=Debug .
cmake --build cmake-build-debug/ --target Schedyl_main
```

## How does it work?

### Data representation
Data representation is described [here](src/bl/io/README.md).

### Creating CNF (Conjunctive Normal Form)
This application uses SAT for finding valid time-disjoint schedules. In order to solve SAT we need SAT solver.

This application uses [Sat4j](https://www.sat4j.org/).

To create CNF formula for getting model representing time-disjoint schedules, we need to make a mapping from scheduling events to integers.
Then we can use those integers to represent variables. Also, we have reverse mapping, i.e. from integers to scheduling events.

We can describe time-disjoint schedule with the following conditions
1. for each scheduling event ```x_0``` find all scheduling events that has a non-empty time intersection ```x_1, x_2, ..., x_n```
2. add CNF representing ```at_most_one(x_0, x_1, ..., x_n)```

Also, we want to select **exactly** one scheduling event of each type from each scheduling subject (types are ```lecture``` or ```lab```).
This is simple. Just add CNF representing for each scheduling subject's scheduling event ```x_1, ..., x_n``` this: ```exactly_one(x_1, ..., x_n)```.

### Evaluation of generated schedule
When we generate schedule we need to calculate the cost. The lower the cost the better the schedule.

Cost features are defined on the top of this page.

# Credits
- [RapidJSON](https://github.com/Tencent/rapidjson)
- [pugixml](https://github.com/zeux/pugixml)
- [HTML Tidy](https://github.com/htacg/tidy-html5)
- [Sat4j](https://www.sat4j.org/)
