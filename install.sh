#!/bin/bash
# run from "schedyl" root folder

# this script just installs all the dependencies

set -e

# check user has 'cmake', 'wget', 'mvn', 'git' and 'sed'
if command -v cmake &>/dev/null && command -v wget &>/dev/null && command -v mvn &>/dev/null && command -v git &>/dev/null && command -v sed &>/dev/null && command -v unzip &>/dev/null
then
  echo "All the dependencies exists, continue..."
else
  echo "Some of the dependencies does not exists."
  exit 1
fi


mkdir vendor
cd vendor/

###################
# download vendor #
###################
git clone https://github.com/google/googletest.git
#git clone https://github.com/zeux/pugixml.git
wget https://github.com/zeux/pugixml/releases/download/v1.11/pugixml-1.11.zip
unzip pugixml-1.11.zip
rm pugixml-1.11.zip


git clone https://github.com/Tencent/rapidjson.git
#git clone https://github.com/htacg/tidy-html5.git
wget https://github.com/htacg/tidy-html5/archive/refs/tags/5.8.0.zip
unzip 5.8.0.zip
rm 5.8.0.zip
mv tidy-html5-5.8.0/ tidy-html5


#################
# prepare sat4j #
#################
wget https://gitlab.ow2.org/sat4j/sat4j/-/archive/2_3_6/sat4j-2_3_6.zip
unzip -qq sat4j-2_3_6.zip
rm sat4j-2_3_6.zip
# we are in vendor/
cd sat4j-2_3_6/org.sat4j.core/
# skip tests -> takes too long, javadoc is also problematic and also we do not want the javadoc :)
mvn clean install -Dmaven.test.skip -Dmaven.javadoc.skip=true

# rename the SAT solver artifact
mv target/org.ow2.sat4j.core-2.3.6.jar target/org.sat4j.core.jar
mkdir ../../sat4j/
cp target/org.sat4j.core.jar ../../sat4j/
# go to vendor/
cd ../../
rm -rf sat4j-2_3_6/

######################
# compile tidy-html5 #
######################
# we are in vendor
mkdir tidy-html5-build/
cd tidy-html5/build/cmake/

# ../.. is vendor/tidy-html5 where is the CMakeLists.txt, DCMAKE_INSTALL_PREFIX = where we want to install the library
# -D__GNU_SOURCE
cmake ../.. -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX=../../../tidy-html5-build
make install

# go to vendor/
cd ../../../

###################################################
# allow pugixml header mode by uncommenting macro #
###################################################
sed -i 's/\/\/ #define PUGIXML_HEADER_ONLY/#define PUGIXML_HEADER_ONLY/' pugixml-1.11/src/pugiconfig.hpp

# go to root of the project folder
#cd ..
#cmake -Bcmake-build-debug/ -DCMAKE_BUILD_TYPE=Debug -G "CodeBlocks - Unix Makefiles" .
#cmake --build cmake-build-debug/ --target Schedyl_main
