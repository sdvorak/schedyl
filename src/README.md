## Definitions

### Terminology

#### General
| English                | Czech                     | Description   |
| :----                  | :----                     | :---          |
| (scheduling) subject   | předmět rozvrhování       | **item** we need to schedule e.g. we want to schedule Mathematical Analysis, or schedule time for learning MA, do homework from MA, ...
| (scheduling) event     | rozvrhový lístek          | **scheduled** *scheduling subject* e.g. Mathematical Analysis lab on Monday between 15:30 - 17:00 on Malostranské náměstí 15 |

#### Scheduling subject
Implementation of **scheduling subject** is just it's unique ID.

#### Scheduling event
Implementation of **scheduling event** is *(scheduling_subject_id, time_range, metadata)*

##### Time range 
Definition of *time range* is *time_range = (dayId, timeFrom, timeTo)*.

*dayId*, *timeFrom* and *timeTo* are defined below in section ***Code lists*** (notice that they are integral values).

#### Metadata
*Metadata* is dictionary for convenience.

Using metadata we are able to make additional constraints. All metadata are **optional**.

| English      | Czech           | Description   |
| :----        | :----           | :---          |
| type         | typ             | e.g. *lecture* (přednáška), *lab* (cvičení) |
| location     | umístění        | **location** of the *scheduling subject* |
| lecturer     | vyučující       | lecturer |

## Business Logic

### Program workflow
1. load data (scheduling events) **from file** (TUI/GUI - maybe in future)
2. load user preferences (maybe in future - preferences are hardcoded in class ``Scheduler.cpp``)
3. generate valid schedule or tell it does not exists
4. evaluate some of the schedules
5. give the best one to the user

### Notes

#### Loading data
There are two types of input data:
- **event data**,
- **user query**,

described in ``bl/io/README.md``, as well as data transformations.

*TODO: Implement user query data interpretation.*


## File Layout
**src**
  - bl (=*business logic*)
    - createschedule
      - Scheduler.cpp   # finding the best schedule
    - io
      - eventdata
      - query
  - util
    - algo
      - SATHelper.cpp   # creating at least, at most and exactly one constraints
    - io
      - progcall
        - ProgCall.cpp  # calls program as in terminal, sets its input and gets its output
    - general
      - GeneralUtil.cpp    # usefull utilities
    - xml
      - XMLUtil.cpp   # searching by XPath, ...
  - plugin
    - SISEventTransformer.cpp  # scraps data from remote (SIS)

**test**
  - Test\[ClassName\].cpp


# Technical details

## Code lists

### Day & Time

#### Day
| day_id  | day     |
| :---    | :---    |
| 1       | MON     |
| 2       | TUE     |
| 3       | WED     |
| 4       | THU     |
| 5       | FRI     |
| 6       | SAT     |
| 7       | SUN     |

### Time
Because we need to index the time intervals by integers, for simplicity we won't handle higher precision than on minutes.

| time_id | from [hh:mm]  | to [hh:mm]  |
| :---    | :---          | :---        |
| 1       | 00:00         | 00:01       |
| 2       | 00:01         | 00:02       |
| ...     | ...           | ...         |
| 1440    | 23:59         | 00:00       |

Check using
```
TIME_POINT_STEP = 1  # time step in minutes
ID = 1
for h in range(0, 24):
    for m in range(0, 60, TIME_POINT_STEP):
        print(h, ':', m, '-', (h + (1 if m+1 == 60 else 0)), ':', (m+1) % 60, 'and the ID=', ID)
        ID += 1
```

### Other metadata examples

#### Location
Consider this

| location_id | location                 |
| :---        | :---                     |
| 1           | Malostranské náměstí 15  |
| 2           | V Holešovičkách          |
| 3           | Ke Karlovu 3             |

We can make constraint on least travelling schedules.

#### Type ID
Consider this

| type_id     | type       |
| :---        | :---       |
| 1           | lecture (přednáška)  |
| 2           | lab (cvičení)    |

We can make constraint on *exactly one* lecture of type *1* of each scheduling subject. Same with *lab* (*2*).

I.e. user needs exactly one Math analysis *lecture* and exactly one Math analysis *lab*, for example.
