//
// Created by simon on 08.02.22.
//

#include "Scheduler.h"

#include <iostream>

#include "../../util/io/progcall/CallSAT.h"
#include "../../util/algo/SATHelper.h"
#include "../../plugin/SISEventTransformer.h"

using namespace std;

bool Scheduler::nextSchedule() {
    // 1. EXACTLY ONE LECTURE & LAB OF EACH SCHEDULING SUBJECT
    // start with excluded schedules
    vector<vector<int>> cnf = excludedSchedules_;

    for (auto& schedulingSubject : edc_.eventsOfSubjects) {
        vector<size_t> eventIdsLecture, eventIdsLab;

        for (Event& e : schedulingSubject.second) {
            size_t eventId = e.eventId;
            string classType = edc_.metaTranslator["classType"].key(e.meta["classType"]);

            if (classType == "lecture") {
                eventIdsLecture.push_back(eventId);
            } else if (classType == "lab") {
                eventIdsLab.push_back(eventId);
            }
        }

        // assert exactly one lecture, if there is any (e.g. Gymnastics does not have a lecture, just lab)
        if (!eventIdsLecture.empty()) {
            vector<int> iEventIdsLecture(eventIdsLecture.begin(), eventIdsLecture.end());
            vector<vector<int>> exactlyOneLecture = SATHelper::exactlyOne(iEventIdsLecture);
            cnf.insert(cnf.end(), exactlyOneLecture.begin(), exactlyOneLecture.end());
        }

        // assert exactly one lab, if there is any
        if (!eventIdsLab.empty()) {
            vector<int> iEventIdsLab(eventIdsLab.begin(), eventIdsLab.end());
            vector<vector<int>> exactlyOneLab = SATHelper::exactlyOne(iEventIdsLab);
            cnf.insert(cnf.end(), exactlyOneLab.begin(), exactlyOneLab.end());
        }
    }

    // 2. NO OVERLAP OF SCHEDULING EVENTS
    // we need to find all colliding scheduling events and say that AT MOST ONE OF THEM can be selected.
    // for the start let's do it by brute-force algorithm
    for (auto& schedulingSubject1 : edc_.eventsOfSubjects) {
        for (Event& e1 : schedulingSubject1.second) {
            for (auto& schedulingSubject2 : edc_.eventsOfSubjects) {
                for (Event& e2 : schedulingSubject2.second) {
                    if (std::addressof(e1) == std::addressof(e2)) continue;

                    if (e1.duration.dayId != e2.duration.dayId) continue;

                    // compare time intersection
                    unique_ptr<Event> starter, other;
                    // THERE MUST BE INTERSECT
                    if (e1.duration.timeFrom.value() == e2.duration.timeFrom.value()) {
                        vector<vector<int>> atMostOne = SATHelper::atMostOne({(int)e1.eventId, (int)e2.eventId});
                        cnf.insert(cnf.end(), atMostOne.begin(), atMostOne.end());
                        continue;
                    } else if (e1.duration.timeFrom.value() < e2.duration.timeFrom.value()) {
                        starter = make_unique<Event>(e1);
                        other = make_unique<Event>(e2);
                    } else {
                        starter = make_unique<Event>(e2);
                        other = make_unique<Event>(e1);
                    }

                    // INTERSECT
                    if (other->duration.timeFrom.value() < starter->duration.timeTo.value()) {
                        vector<vector<int>> atMostOne = SATHelper::atMostOne({(int)starter->eventId, (int)other->eventId});
                        cnf.insert(cnf.end(), atMostOne.begin(), atMostOne.end());
                    }
                }
            }
        }
    }

    // 2.5 EMPTY DAY
    // iterate over all the empty days
    for (DayId emptyDayId : uco_.emptyDayId) {
        for (auto &schedulingSubject : edc_.eventsOfSubjects) {
            for (Event &e : schedulingSubject.second) {
                if (e.duration.dayId == emptyDayId) {
                    cnf.push_back({-(int) e.eventId});
                }
            }
        }
    }

    // 2.6 EMPTY GAPS
    for (TimeRange emptyTimeGap : uco_.emptyGap) {
        for (auto &schedulingSubject : edc_.eventsOfSubjects) {
            for (Event &e : schedulingSubject.second) {
                // check current event collides with some empty time gap
                if (e.duration.dayId == emptyTimeGap.dayId && // same day
                    (
                        ( // event "starts" between the timeGap
                            e.duration.timeFrom.value() >= emptyTimeGap.timeFrom.value() && // event starts or later than time gap...
                            e.duration.timeFrom.value() <= emptyTimeGap.timeTo.value() // ...and starts before the end of the time gap. OR
                        ) || ( // OR event "ends" between timeGap
                            e.duration.timeTo.value() >= emptyTimeGap.timeFrom.value() &&
                            e.duration.timeTo.value() <= emptyTimeGap.timeTo.value()
                        )
                    )
                ) {
                    cnf.push_back({-(int) e.eventId});
                }
            }
        }
    }

    // 3. WRITE CNF FILE
    SATHelper::writeFile(cnfFilename_, edc_.getSchedulingEventCount(), cnf);


    // 4. SOLVE
    vector<int> solution;
    bool result = CallSAT::callSAT(cnfFilename_, solution);

    //cout << (result ? "SATISFIABLE" : "UNSATISFIABLE") << endl << "***********" << endl << endl;

    if (!result) return false;

    // assert: solution exists

    // exclude this 'solution' from nextSchedule call.
    vector<int> toExclude = solution;
    transform(toExclude.begin(), toExclude.end(), toExclude.begin(), [](size_t x) { return -x; });

    excludedSchedules_.push_back(toExclude);

    vector<size_t> solutionEventIds;
    copy_if(solution.begin(), solution.end(), back_inserter(solutionEventIds), [](int x) { return x > 0; });
    currentSchedule_ = solutionEventIds;

    // 6. CALCULATE COST AND IF THIS SCHEDULE IS BETTER, THAN SET IT AS THE BEST
    curScheduleCost_ = cost();
    if (curScheduleCost_ < bestScheduleCost_) {
        bestSchedule_ = currentSchedule_;
        bestScheduleCost_ = curScheduleCost_;
    }

    return true;
}

int Scheduler::cost() {
    map<DayId, vector<shared_ptr<Event>>> days;
    for (size_t curEventId : currentSchedule_) {
        DayId curDayId = edc_.eventIdToEvent[curEventId]->duration.dayId;
        days[curDayId].push_back(edc_.eventIdToEvent[curEventId]);
    }

    // 1. START BY LEAST TIME GAPS (if user wants it)
    int timeGaps = 0;
    if (uco_.leastTimeGaps) {
        // calculate time gaps between scheduling events in minutes

        for (auto &p : days) {
            sort(p.second.begin(), p.second.end(), [](shared_ptr<Event> &e1, shared_ptr<Event> &e2) {
                return e1->duration.timeFrom.value() < e2->duration.timeFrom.value();
            });
        }

        for (auto &p : days) {
            bool firstIteration = true;
            int prevEnd;

            for (shared_ptr<Event> &e : p.second) {
                if (firstIteration) {
                    prevEnd = e->duration.timeTo.value();
                    firstIteration = false;
                    continue;
                }

                timeGaps += e->duration.timeFrom.value() - prevEnd;
                prevEnd = e->duration.timeTo.value();
            }
        }
    }

    // 2. LEAST TRANSFERS (if user wants it)
    int transferCount = 0;
    const int transferCoefficient = 50; // 50 min to transfer (avg)

    if (uco_.leastTravelling) {
        for (auto &p : days) {
            bool firstIteration = true;
            SISEventTransformer::locations_ prevEndBuildingId;

            for (shared_ptr<Event> &e : p.second) {
                if (firstIteration) {
                    prevEndBuildingId = (SISEventTransformer::locations_) stoi(
                            edc_.metaTranslator["location"].key(e->meta["location"]));
                    firstIteration = false;
                    continue;
                }

                auto curBuildingId = (SISEventTransformer::locations_) stoi(
                        edc_.metaTranslator["location"].key(e->meta["location"]));

                if (curBuildingId != prevEndBuildingId) {
                    ++transferCount;
                }

                prevEndBuildingId = curBuildingId;
            }
        }
    }

    int totalCost = transferCoefficient * transferCount + timeGaps;
    return totalCost;
}

void Scheduler::dumpCurrentSchedule() {
    cout << "COST: " << bestScheduleCost_ << endl;

    for (event_id eventId : currentSchedule_) {
        if (eventId <= 0) continue;

        shared_ptr<Event> e = edc_.eventIdToEvent[eventId];
        edc_.dumpEvent(*e);
    }
}
