//
// Created by simon on 08.02.22.
//

#ifndef SCHEDYL_SCHEDULER_H
#define SCHEDYL_SCHEDULER_H

#include <iostream>
#include <climits>
#include "../../util/struct/MyStructures.h"
#include "../io/query/JsonQuery.h"
#include "../io/eventdata/EventDataContainer.h"
#include "../io/eventdata/JsonEventTransformer.h"

class Scheduler {
public:
    /**
     * TODO: use temporary file instead of user given file for CNF.
     * https://en.cppreference.com/w/cpp/io/c/tmpfile
     * @param edc
     */
    Scheduler(const EventDataContainer& edc, const UserQuery& uco, const std::string& cnfFilename):
        edc_(edc), uco_(uco), cnfFilename_(cnfFilename) {}

    /**
     * Create next schedule and then exclude it from valid schedules in order to find other schedules.
     * @return true, if found next schedule, false if next schedule does not exist.
     */
    bool nextSchedule();

    int cost();

    void dumpCurrentSchedule();
    int getCurrentCost() const {
        return curScheduleCost_;
    }

    int getBestCost() const {
        return bestScheduleCost_;
    }

private:
    /**
     * From constructor
     */
    EventDataContainer edc_;
    UserQuery uco_;
    std::string cnfFilename_;

    /**
     * Others
     */
    std::vector<std::vector<int>> excludedSchedules_;

    /**
     * Event ids that form the current schedule. (Negative values from SAT are already filtered!)
     */
    std::vector<event_id> currentSchedule_;

    int bestScheduleCost_ = INT_MAX;
    int curScheduleCost_ = bestScheduleCost_;
    std::vector<event_id> bestSchedule_;
};


#endif //SCHEDYL_SCHEDULER_H
