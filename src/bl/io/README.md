# Data

There is some abstraction around data management. We divide data into 2 types:
- **event data** - data containing *scheduling events*,
- **user query** - constraints forming the query.

The process of getting data and transforming it to the uniform form is described below.

## Transformation

### Event data
1. reading data, i.e. JSON files, HTML from third-party source, whatever you implement,
2. passing read data (from step 1.) to an appropriate **transformer**,
3. transformer produces uniform format (``EventDataContainer``) which consists of
    - mappings
        - scheduling_subject_id <-> scheduling_subject_name
        - scheduling_subject_id <-> vector\<Event\>
        - meta_name -> (value <-> ID)
