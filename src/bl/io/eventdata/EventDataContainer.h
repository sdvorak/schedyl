//
// Created by simon on 09.02.22.
//

#ifndef SCHEDYL_EVENTDATACONTAINER_H
#define SCHEDYL_EVENTDATACONTAINER_H

#include <iostream>
#include "../../../util/struct/MyStructures.h"
#include "../../../util/general/GeneralUtil.h"

/**
 * Uniform <b>event data</b> container.
 */
class EventDataContainer {
public:
    /**
     * Name of the scheduling subject, e.g. "Mathematical Analysis II".
     */
    typedef std::string scheduling_subject_name;

    /**
     * Mapping from 'subject name' to it's unique generated ID, e.g. 'Math analysis II' -> 1
     * Translations work in both directions.
     */
    GeneralUtil::Translator<scheduling_subject_name, sch_subj_id> schedulingSubjectTranslator;

    /**
     * Mapping from scheduling subject id to it's scheduling events
     */
    std::map<sch_subj_id, std::vector<Event>> eventsOfSubjects;

    /**
     * Mapping from event_id to the corresponding Event object
     */
    std::map<event_id, std::shared_ptr<Event>> eventIdToEvent;

    /**
     * Metadata name.
     */
    typedef std::string meta_name;

    /**
     * Metadata value.
     */
    typedef std::string meta_value;

    /**
     * Metadata ID.
     */
    typedef size_t metaId;

    /**
     * Mapping handling translation between IDs and strings of real values.
     * e.g.
     * USAGE    adding          int classTypeId = metaTranslator["classType"].add(class_type)
     *          translating     metaTranslator["classType"].key(e.meta["classType"])
     */
    std::map<meta_name, GeneralUtil::Translator<meta_value, metaId>> metaTranslator;

    /**
     * Get scheduling event count.
     * @return scheduling event count
     */
    size_t getSchedulingEventCount() {
        return eventIdToEvent.size();
    }

    /**
     * Dump specified event.
     * @param e     Event to dump
     */
    void dumpEvent(Event& e);

    /**
     * Dump whole structure.
     */
    void dump();

    /**
     * Dump whole structure in JSON format.
     */
    void toJSON();
private:
};


inline void EventDataContainer::dumpEvent(Event& e) {
    std::cout << "subject name:\t" << schedulingSubjectTranslator.key(e.schedulingSubjectId) << std::endl;
    std::cout << "duration:\t\t" << e.duration << std::endl;
    for (auto& p : e.meta) {
        std::string metaName = p.first;
        size_t metaKey = p.second;
        std::cout << metaName << ":\t\t" << metaTranslator[metaName].key(metaKey) << std::endl;
    }

    std::cout << std::endl;
}

inline void EventDataContainer::dump() {
    for (auto& schedulingSubject : eventsOfSubjects) {
        size_t schedulingSubjectId = schedulingSubject.first;
        std::string schedulingSubjectName = schedulingSubjectTranslator.key(schedulingSubject.first);
        std::cout << "Scheduling subject name: \"" << schedulingSubjectName << "\" (ID: " << schedulingSubjectId << "):" << std::endl;
        for (Event& e : schedulingSubject.second) {
            std::cout << "\tEVENT ID: " << e.eventId << std::endl;
            std::cout << "\tDURATION: " << e.duration << std::endl;
            std::cout << "\tLECTURER: " << metaTranslator["lecturer"].key(e.meta["lecturer"]) << " (" << e.meta["lecturer"] << ")" << std::endl;
            std::cout << "\tLOCATION: " << metaTranslator["location"].key(e.meta["location"]) << " (" << e.meta["location"] << ")" << std::endl;
            std::cout << "\tCLASS TYPE: " << metaTranslator["classType"].key(e.meta["classType"]) << " (" << e.meta["classType"] << ")" << std::endl;

            std::cout << std::endl;
        }
        std::cout << std::endl << "*********************" << std::endl << std::endl;
    }
}

inline void EventDataContainer::toJSON() {
    std::cout << "{" << std::endl;
    for (auto &schedulingSubject : eventsOfSubjects) {
        std::string schedulingSubjectName = schedulingSubjectTranslator.key(schedulingSubject.first);
        std::cout << "\"" << schedulingSubjectName << "\": [" << std::endl;
        for (Event &e : schedulingSubject.second) {
            std::cout << "\t{" << std::endl;
            std::cout << "\t\t\"timeRange\": [" << (int) e.duration.dayId << ", " << e.duration.timeFrom.value() << ", "
                 << e.duration.timeTo.value() << "]," << std::endl;
            std::cout << "\t\t\"lecturer\": \"" << metaTranslator["lecturer"].key(e.meta["lecturer"]) << "\","
                 << std::endl;
            std::cout << "\t\t\"location\": \"" << metaTranslator["location"].key(e.meta["location"]) << "\","
                 << std::endl;
            std::cout << "\t\t\"classType\": \"" << metaTranslator["classType"].key(e.meta["classType"]) << "\""
                 << std::endl;
            std::cout << "\t}," << std::endl;
        }
        std::cout << "]," << std::endl;
    }
    std::cout << "}" << std::endl;
}


#endif //SCHEDYL_EVENTDATACONTAINER_H
