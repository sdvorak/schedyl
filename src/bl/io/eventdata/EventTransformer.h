//
// Created by simon on 09.02.22.
//

#ifndef SCHEDYL_EVENTTRANSFORMER_H
#define SCHEDYL_EVENTTRANSFORMER_H

#include "EventDataContainer.h"

/**
 *
 * @tparam I    'I' as an 'Input type' - it can be filename (i.e. std::string), std::string from command prompt,
 *              or other resource.
 */
template <class I>
class EventTransformer {
public:
    /**
     * Will be called before each transform() call.
     * @param parameters
     */
    virtual void getData([[maybe_unused]] I& parameters) {
        canCall_ = true;
    }

    /**
     * Function that transforms from one format (e.g. JSON), given as 'I' to the uniform output format, which is always 'EventDataContainer'.
     * TODO: throw more specific exceptions to be able to handle them separately.
     * @throws runtime_error
     */
    virtual void transform() {
        if (!canCall_) throw std::runtime_error("Can't call transform twice without calling getData().");
        canCall_ = false;

        if (!validate()) {
            throw std::runtime_error("Input data is not valid.");
        }
    }

    /**
     * Getter of the transformation.
     * @return uniform format of scheduling event data
     */
    virtual EventDataContainer getTransformation() = 0;

    /**
     * Input validator.
     * @return true, if event data are in correct format, false otherwise.
     */
    virtual bool validate() = 0;

protected:
    /**
     * Current scheduling event ID to assign, if new event is parsed.
     */
    size_t event_id_ = 1;

private:
    /**
     * User can call transform() iff getData() was called.
     */
    bool canCall_ = false;
};

#endif //SCHEDYL_EVENTTRANSFORMER_H
