//
// Created by simon on 09.02.22.
//

#include "JsonEventTransformer.h"

#include <iostream>
#include <algorithm>
#include <string>

#include <rapidjson/document.h>
#include <rapidjson/schema.h>

#include "EventDataContainer.h"

using namespace std;

void JsonEventTransformer::transform() {
    // call parent constructor
    EventTransformer::transform();

    ifstream f(filename_);
    string jsonFile = GeneralUtil::getFileContents(f);

    rapidjson::Document d;
    d.Parse(jsonFile.c_str());

    for(const auto& schedulingSubject : d.GetObject()) {
        string schedulingSubjectName = schedulingSubject.name.GetString();
        size_t schedulingSubjectId = edc_.schedulingSubjectTranslator.add(schedulingSubjectName);

        vector<Event> events;

        for(const auto& schedulingEvent : schedulingSubject.value.GetArray()) {
            // 'timeRange' is certainly here because of JSON Schema validation
            auto timeRange = schedulingEvent["timeRange"].GetArray();
            // we know that timeRange is of size 3, because of JSON Schema validation
            auto dayId = (DayId) timeRange[0].GetInt();
            auto timeFrom = (TimePoint) timeRange[1].GetInt();
            auto timeTo = (TimePoint) timeRange[2].GetInt();
            TimeRange tr(dayId, timeFrom, timeTo);

            map<string, size_t> meta;

            for (const auto& c : schedulingEvent.GetObject()) {
                auto metaNameRapidJson = c.name.GetString();
                string metaName = string(metaNameRapidJson);
                if (metaName != "timeRange") {
                    string metaValue = string(schedulingEvent[metaNameRapidJson].GetString());
                    size_t metaId = edc_.metaTranslator[metaName].add(metaValue);
                    meta[string(c.name.GetString())] = metaId;
                }
            }


            Event e(event_id_, schedulingSubjectId, tr, meta);
            events.push_back(e);

            edc_.eventIdToEvent.insert(make_pair(event_id_, make_shared<Event>(e)));

            ++event_id_;
        }

        edc_.eventsOfSubjects[schedulingSubjectId] = events;
    }

}

void JsonEventTransformer::getData(string& parameters) {
    // call parent constructor
    EventTransformer::getData(parameters);
    filename_ = parameters;
}

bool JsonEventTransformer::validate() {
    // https://rapidjson.org/md_doc_schema.html
    rapidjson::Document sd;

    ifstream f("src/bl/io/eventdata/eventdata-json-schema.json");
    string schemaContent = GeneralUtil::getFileContents(f);
    if (sd.Parse(schemaContent.c_str()).HasParseError()) {
        throw runtime_error("The schema is not a valid JSON.");
    }

    rapidjson::SchemaDocument schema(sd); // Compile a Document to SchemaDocument
    // sd is no longer needed here.

    rapidjson::Document d;

    ifstream ff(filename_);

    if (!ff.good()) {
        throw runtime_error("File does not exist.");
    }

    string fileContent = GeneralUtil::getFileContents(ff);
    if (d.Parse(fileContent.c_str()).HasParseError()) {
        throw runtime_error("The input is not a valid JSON.");
    }

    rapidjson::SchemaValidator validator(schema);
    if (!d.Accept(validator)) {
        // Input JSON is invalid according to the schema
        // Output diagnostic information
        rapidjson::StringBuffer sb;
        validator.GetInvalidSchemaPointer().StringifyUriFragment(sb);
        printf("Invalid schema: %s\n", sb.GetString());
        printf("Invalid keyword: %s\n", validator.GetInvalidSchemaKeyword());
        sb.Clear();
        validator.GetInvalidDocumentPointer().StringifyUriFragment(sb);
        printf("Invalid document: %s\n", sb.GetString());
        return false;
    }

    return true;
}
