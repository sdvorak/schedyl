//
// Created by simon on 09.02.22.
//

#ifndef SCHEDYL_JSONEVENTTRANSFORMER_H
#define SCHEDYL_JSONEVENTTRANSFORMER_H

#include <map>
#include <vector>

#include "../../../util/struct/MyStructures.h"
#include "EventDataContainer.h"
#include "EventTransformer.h"

/**
 * 'std::string' template parameter in 'EventTransformer\<I\>' means we are going to load data from file.
 */
class JsonEventTransformer : public EventTransformer<std::string> {
public:
    explicit JsonEventTransformer() = default;

    void transform() override;
    void getData(std::string& parameters) override;

    EventDataContainer getTransformation() override {
        return edc_;
    }

    bool validate() override;

private:
    EventDataContainer edc_;

    std::string filename_;
};


#endif //SCHEDYL_JSONEVENTTRANSFORMER_H
