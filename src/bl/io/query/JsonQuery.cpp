//
// Created by simon on 17.11.21.
//


#include <iostream>
#include <string>
#include <algorithm>

#include "rapidjson/document.h"
#include "rapidjson/stringbuffer.h"

#include "JsonQuery.h"
#include "../../../util/general/GeneralUtil.h"

using namespace std;

UserQuery JsonQuery::loadConfig(const std::string &fileName) {
    // load from the file
    ifstream f(fileName);
    string jsonFile = GeneralUtil::getFileContents(f);

    // parse
    rapidjson::Document d;
    d.Parse(jsonFile.c_str());

    vector<DayId> emptyDayId;
    transform(d["dayOffId"].GetArray().begin(), d["dayOffId"].GetArray().end(), back_inserter(emptyDayId), [](auto &x) {
        return (DayId)x.GetInt();
    });

    vector<TimeRange> emptyGap;
    transform(d["emptyGap"].GetArray().begin(), d["emptyGap"].GetArray().end(), back_inserter(emptyGap), [](auto &x) {
        // TODO: check the DayId and other values
        assert(x.GetArray().Size() == 3);
        TimeRange tr((DayId)x.GetArray()[0].GetInt(), x.GetArray()[1].GetInt(), x.GetArray()[2].GetInt());
        return tr;
    });

    vector<pair<int, int>> lecturerPreference;
    transform(d["lecturerPreference"].GetArray().begin(), d["lecturerPreference"].GetArray().end(), back_inserter(lecturerPreference), [](auto &x) {
        assert(x.GetArray().Size() == 2);
        pair<int, int> curLecturerPreference = make_pair(x.GetArray()[0].GetInt(), x.GetArray()[1].GetInt());
        return curLecturerPreference;
    });

    UserQuery uco(emptyGap, emptyDayId, lecturerPreference, d["leastTravelling"].GetBool(), d["leastTimeGaps"].GetBool());
    return uco;
}

UserQuery JsonQuery::loadConfig() {
    std::vector<TimeRange> emptyGap = { TimeRange(TUE, 780, 870) }; // TUE = Tuesday && 780 = 13:00 && 870 = 14:30
    std::vector<DayId> emptyDayId = { };
    // not yet implemented
    std::vector<std::pair<int, int>> lecturerPreference;
    bool leastTravelling = true;
    bool leastTimeGaps = true;

    UserQuery uco(emptyGap, emptyDayId, lecturerPreference, leastTravelling, leastTimeGaps);
    return uco;
}
