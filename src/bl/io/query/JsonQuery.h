//
// Created by simon on 17.11.21.
//

#ifndef SCHEDYL_JSONQUERY_H
#define SCHEDYL_JSONQUERY_H

#include <vector>
#include <string>
#include "../../../util/struct/MyStructures.h"

class JsonQuery {
public:
    /**
     * Load from JSON file.
     * @param fileName File containing user preferences in JSON format.
     * @return object representing parsed file
     */
    static UserQuery loadConfig(const std::string& fileName);

    /**
     * User did not specify any file so let's use default configuration.
     * @return object representing parsed file
     */
    static UserQuery loadConfig();
private:

};


#endif //SCHEDYL_JSONQUERY_H
