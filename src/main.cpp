#include <iostream>

#include "bl/createschedule/Scheduler.h"
#include "plugin/SISEventTransformer.h"


using namespace std;

int main(int argc, char* argv[]) {
    // user will specify absolute path...
    string inFilename, criteriaFilename, cnfFilename;
    string sisFilename;

    vector<string> commandLineArguments(argv + 1, argv + argc);

    if (commandLineArguments.empty()) {
        cerr << "At least 1 argument must be specified." << endl;
        exit(1);
    }

    /*
- find
  - \[file with scheduling events\].json \[criteria\].json \[temporary cnf file\].cnf
- plugin_sis_transform
  - \[file.html\]
     */

    shared_ptr<UserQuery> uco;
    if (commandLineArguments[0] == "find") {
        if (commandLineArguments.size() == 3) {
            inFilename = commandLineArguments[1];
            cnfFilename = commandLineArguments[2];
            uco = make_shared<UserQuery>(JsonQuery::loadConfig());
        } else if (commandLineArguments.size() == 4) {
            inFilename = commandLineArguments[1];
            criteriaFilename = commandLineArguments[2];
            cnfFilename = commandLineArguments[3];

            uco = make_shared<UserQuery>(JsonQuery::loadConfig(criteriaFilename));
        } else {
            cerr << "Invalid number of arguments. Expected 3 or 4, got " + to_string(commandLineArguments.size()) << endl;
            exit(1);
        }

        JsonEventTransformer jet;

        jet.getData(inFilename);
        jet.transform();

        EventDataContainer edc = jet.getTransformation();

        Scheduler s(edc, *uco, cnfFilename);

        int ctr = 0;
        while (s.nextSchedule()) {
            if (s.getCurrentCost() == s.getBestCost()) {
                s.dumpCurrentSchedule();
            }

            ++ctr;
        }

        cout << "**************************" << endl;
        cout << "Total number of schedules: " << ctr << "." << endl;
    } else if (commandLineArguments[0] == "plugin_sis_transform") {
        if (commandLineArguments.size() == 2) {
            sisFilename = commandLineArguments[1];

            SISEventTransformer set;
            set.getData(sisFilename);
            if (set.validate()) {
                set.transform();
                EventDataContainer edc = set.getTransformation();
                edc.toJSON();
            } else {
                cerr << "Probably invalid format of SIS data." << endl;
                exit(1);
            }
        } else {
            cerr << "Invalid number of arguments. Expected 2, got " + to_string(commandLineArguments.size()) << endl;
            exit(1);
        }
    } else {
        cerr << "Unknown operation: " + commandLineArguments[0] << endl;
        exit(1);
    }

    return 0;
}
