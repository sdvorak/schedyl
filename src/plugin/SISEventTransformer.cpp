//
// Created by simon on 05.12.21.
//

#include "SISEventTransformer.h"

#include "../util/xml/XMLUtil.h"

using namespace std;

void SISEventTransformer::transform() {
    EventTransformer::transform();

    ifstream f(filename_);

    string content = GeneralUtil::getFileContents(f);

    string tidied = XMLUtil::cleanHTML(content);

    pugi::xml_document doc;
    pugi::xml_parse_result result = doc.load_string(tidied.c_str());
    if (!result) {
        throw runtime_error("Can't parse given file.");
    }

    const string xpath = "//html/body/table/tbody/tr/td/div[1]/table[3]/tbody/tr";
    const pugi::xpath_node_set nodes = doc.select_nodes(xpath.c_str());



    vector<Event> events;
    size_t schedulingSubjectId;
    string schedulingSubjectName;

    for(auto& node: nodes) {
        string class_type = XMLUtil::getTextByXPath(node.node(), "td[2]/span");

        if (class_type.empty()) {
            continue;
        }

        if (class_type.length() == 1) {
            char type = class_type[0];

            switch (type) {
                case 'P':
                    class_type = "lecture";
                    break;
                case 'X':
                    class_type = "lab";
                    break;
                default:
                    throw std::runtime_error("This class type is not known \"" + std::to_string(type) + "\".");
            }
        }

        //////////////////////////////////////////////////////////////////////
        string subject_name = XMLUtil::getTextByXPath(node.node(), "td[3]/a/span");

        if (subject_name.empty()) {
            subject_name = XMLUtil::getTextByXPath(node.node(), "td[3]/a");
        }
        //////////////////////////////////////////////////////////////////////

        std::string day = XMLUtil::getTextByXPath(node.node(), "td[5]");

        stringstream ss(day);
        string dayName;
        ss >> dayName;
        string dayTime;
        ss >> dayTime;
        //////////////////////////////////////////////////////////////////////

        std::string location = XMLUtil::getTextByXPath(node.node(), "td[6]");

        //////////////////////////////////////////////////////////////////////

        std::string teacher = XMLUtil::getTextByXPath(node.node(), "td[4]");
        //////////////////////////////////////////////////////////////////////

        if (subject_name.empty() ||
            class_type.empty() ||
            dayName.empty() ||
            dayTime.empty() ||
            teacher.empty()) {
            // TODO: show error
            continue;
        }

        if (schedulingSubjectName.empty() && !subject_name.empty()) {
            subject_name = GeneralUtil::strip(subject_name);
            GeneralUtil::replaceCharacter(subject_name, "\n", " ");
            schedulingSubjectName = subject_name;
            schedulingSubjectId = edc_.metaTranslator["subject"].add(subject_name);
        }

        location = GeneralUtil::strip(location);
        locations_ locationEnum = translateRoomToPlace(location);
        string sLocationEnum = to_string(locationEnum);

        teacher = GeneralUtil::strip(teacher);

        size_t classTypeId = edc_.metaTranslator["classType"].add(class_type);
        size_t lecturerId = edc_.metaTranslator["lecturer"].add(teacher);
        //int locationId = edc_.metaTranslator["location"].add(location);
        size_t locationId = edc_.metaTranslator["location"].add(sLocationEnum);

        TimePoint start = GeneralUtil::timeToTimePoint(dayTime);
        TimePoint end = GeneralUtil::timeToTimePoint(dayTime) + 90; // 90 => 90 min = 1h 30min

        TimeRange tr(GeneralUtil::dayNameToDayId(dayName), start, end);

        map<string, size_t> meta;
        meta["classType"] = classTypeId;
        meta["lecturer"] = lecturerId;
        meta["location"] = locationId;

        Event e(event_id_, schedulingSubjectId, tr, meta);
        events.push_back(e);

        edc_.eventIdToEvent.insert(make_pair(event_id_, make_shared<Event>(e)));

        ++event_id_;
    }

    edc_.eventsOfSubjects[schedulingSubjectId] = events;
    edc_.schedulingSubjectTranslator.add(schedulingSubjectName);
}

void SISEventTransformer::getData(string &parameters) {
    EventTransformer::getData(parameters);
    filename_ = parameters;
}

bool SISEventTransformer::validate() {
    // TODO: implement validation
    return true;
}
