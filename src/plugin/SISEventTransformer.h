//
// Created by simon on 05.12.21.
//

#ifndef SCHEDYL_SISEVENTTRANSFORMER_H
#define SCHEDYL_SISEVENTTRANSFORMER_H

#include <iostream>
#include <fstream>
#include <sstream>
#include "../util/general/GeneralUtil.h"
#include "../bl/io/eventdata/EventDataContainer.h"
#include "../bl/io/eventdata/EventTransformer.h"

class SISEventTransformer : public EventTransformer<std::string> {
public:
    explicit SISEventTransformer() = default;

    void transform() override;
    void getData(std::string& parameters) override;

    EventDataContainer getTransformation() override {
        return edc_;
    }

    bool validate() override;

    enum locations_ {IMPAKT, MALOSTRANSKE_NAMESTI, KE_KARLOVU, TELOCVIK, UNKNOWN};

private:
    EventDataContainer edc_;

    std::string filename_;

    static locations_ translateRoomToPlace(const std::string& room) {
        // IMPAKT
        if (GeneralUtil::startsWith(room, "N")) {
            return IMPAKT;
        }
        // Malostranske namesti
        else if (GeneralUtil::startsWith(room, "S")) {
            return MALOSTRANSKE_NAMESTI;
        }
        // Ke Karlovu
        else if (GeneralUtil::startsWith(room, "M")) {
            return KE_KARLOVU;
        }
        // Telocvik
        else if (GeneralUtil::startsWith(room, "KTV")) {
            return TELOCVIK;
        }

        return UNKNOWN;
    }
};


#endif //SCHEDYL_SISEVENTTRANSFORMER_H
