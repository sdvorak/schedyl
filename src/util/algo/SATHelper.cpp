//
// Created by simon on 09.02.22.
//

#include "SATHelper.h"

#include <iostream>
#include <fstream>

using namespace std;

std::vector<int> SATHelper::atLeastOne(const std::vector<int> &literals) {
    return std::vector<int>(literals);
}

std::vector<vector<int>> SATHelper::atMostOne(const std::vector<int> &literals) {
    std::vector<vector<int>> result;

    for (auto it1 = literals.begin(); it1 != literals.end(); ++it1) {
        for (auto it2 = it1+1; it2 != literals.end(); ++it2) {
            if (it1 == it2) continue;
            vector<int> pair;
            pair.push_back(-*it1);
            pair.push_back(-*it2);
            result.push_back(pair);
        }
    }

    return result;
}

std::vector<std::vector<int>> SATHelper::exactlyOne(const std::vector<int> &literals) {
    vector<vector<int>> result;

    result.push_back(atLeastOne(literals));
    vector<vector<int>> amo = atMostOne(literals);
    result.insert(result.end(), amo.begin(), amo.end());

    return result;
}

void SATHelper::writeFile(const string &outFilename, size_t variableCount, const vector<std::vector<int>> &cnf) {
    ofstream outFile;

    string filename = outFilename.substr(outFilename.find_last_of('/') + 1);

    outFile.open(outFilename);

    outFile << "c " << outFilename << "\n";
    outFile << "c \n";
    outFile << "c \n";
    outFile << "c \n";
    size_t clauseCount = cnf.size();
    outFile << "p cnf " << variableCount << " " << clauseCount << "\n\n\n";

    for (auto& clause : cnf) {
        for (int literal : clause) {
            outFile << literal << " ";
        }
        outFile << "0\n";
    }

    outFile.close();
}
