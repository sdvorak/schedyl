//
// Created by simon on 09.02.22.
//

#ifndef SCHEDYL_SATHELPER_H
#define SCHEDYL_SATHELPER_H

#include <iostream>
#include <vector>

class SATHelper {
public:
    static std::vector<int> atLeastOne(const std::vector<int>& literals);
    static std::vector<std::vector<int>> atMostOne(const std::vector<int>& literals);
    static std::vector<std::vector<int>> exactlyOne(const std::vector<int>& literals);


    static void writeFile(const std::string& outFilename, size_t variableCount, const std::vector<std::vector<int>>& cnf);

private:
};


#endif //SCHEDYL_SATHELPER_H
