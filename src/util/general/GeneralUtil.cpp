//
// Created by simon on 11.12.21.
//

#include "GeneralUtil.h"

#include <string>
#include <algorithm>

#include "../struct/MyStructures.h"

void GeneralUtil::canonicalize(std::string& str) {
    std::transform(str.begin(), str.end(), str.begin(), ::tolower);
}

DayId GeneralUtil::dayNameToDayId(const std::string& dayName) {
    std::string cpy = dayName;
    canonicalize(cpy);

    if (dayName == "Po" || dayName == "po") {
        return MON;
    } else if (dayName == "Út" || dayName == "ut") {
        return TUE;
    } else if (dayName == "St" || dayName == "st") {
        return WED;
    } else if (dayName == "Čt" || dayName == "čt") {
        return THU;
    } else if (dayName == "Pá" || dayName == "pá") {
        return FRI;
    } else if (dayName == "So" || dayName == "so") {
        return SAT;
    } else if (dayName == "Ne" || dayName == "ne") {
        return SUN;
    } else {
        throw std::runtime_error("Unknown day name");
    }
}

std::vector<std::string> GeneralUtil::tokenize(const std::string &s, const std::string& del) {
    std::vector<std::string> tokens;
    size_t start = 0;
    size_t end = s.find(del);
    while (end != std::string::npos) {
        tokens.push_back(s.substr(start, end - start));
        start = end + del.size();
        end = s.find(del, start);
    }

    tokens.push_back(s.substr(start, end - start));

    return tokens;
}

bool GeneralUtil::startsWith(const std::string& str, const std::string& with) {
    if (str.rfind(with, 0) == 0) {
        return true;
    }

    return false;
}

TimePoint GeneralUtil::timeToTimePoint(const std::string &time) {
    std::vector<std::string> tokens = tokenize(time, ":");
    if (tokens.size() == 1 && tokens[0].empty()) {
        throw std::runtime_error("Time was not specified.");
    }

    if (tokens.size() != 2) {
        throw std::runtime_error("Time \"" + time + "\" is not valid (too many ':' characters).");
    }

    std::string hour, minute;
    hour = tokens[0];
    minute = tokens[1];

    if (!isNaturalNumber(hour) || !isNaturalNumber(minute)) {
        throw std::runtime_error("Time \"" + time + "\" is not valid (either hours or minutes were not natural numbers).");
    }

    try {
        int iHour = std::stoi(hour);
        int iMinute = std::stoi(minute);
        TimePoint start(iHour * 60 + iMinute);
        return start;
    } catch (std::out_of_range& e) {
        std::cout << e.what() << std::endl;
    }

    return -1;
}

std::string GeneralUtil::strip(const std::string &str) {
    auto start_it = str.begin();
    auto end_it = str.rbegin();

    while (std::isspace(*start_it) || *start_it == '\n') {
        ++start_it;
    }

    while (std::isspace(*end_it) || *end_it == '\n') {
        ++end_it;
    }

    return std::string(start_it, end_it.base());
}

void GeneralUtil::removeNewlines(std::string &str) {
    str.erase(std::remove(str.begin(), str.end(), '\n'), str.end());
}

void GeneralUtil::replaceCharacter(std::string &str, const std::string &toReplace, const std::string &with) {
    str = std::regex_replace(str, std::regex(toReplace), with);
}
