//
// Created by simon on 11.12.21.
//

#ifndef SCHEDYL_GENERALUTIL_H
#define SCHEDYL_GENERALUTIL_H

#include <iostream>
#include <algorithm>
#include <vector>
#include <map>
#include <fstream>
#include <regex>

#include <experimental/filesystem>

#include "../struct/MyStructures.h"

class GeneralUtil {
public:
    /**
     * https://stackoverflow.com/questions/2912520/read-file-contents-into-a-string-in-c
     * @param   f   file given as ifstream object
     * @return      file 'f' as string.
     */
    static std::string getFileContents(std::ifstream& f) {
        if (!f.good()) {
            throw std::runtime_error("Filename not valid.");
        }

        return std::string(
                std::istreambuf_iterator<char>(f),
                std::istreambuf_iterator<char>()
        );
    }

    /**
     * Convert string 'str' to lowercase.
     * @param   str     string to convert to lowercase
     */
    static void canonicalize(std::string& str);

    /**
     * TODO: refactor.
     * Mapping from dayName
     * @param       dayName     Czech name of the day
     * @return                  ID of day of type DayId
     */
    static DayId dayNameToDayId(const std::string& dayName);

    /**
     * Splits string 's' by delimiter 'del'.
     * @param       s       string we want to tokenize
     * @param       del     delimiter
     * @return              tokenized string
     */
    static std::vector<std::string> tokenize(const std::string& s, const std::string& del = " ");

    /**
     * Checks if string 'str' starts with string 'with'.
     * @param str   string to check
     * @param with  starting string
     * @return      true, if 'str' starts with 'with', false otherwise.
     */
    static bool startsWith(const std::string& str, const std::string& with);

    /**
     * Check if string is just representation of natural number.
     * @param       str     string to check
     * @return      true, if is natural, false otherwise
     */
    static bool isNaturalNumber(const std::string& str) {
        return !str.empty() && std::all_of(str.begin(), str.end(), ::isdigit);
    }

    /**
     * Convertor of time in format 'hh:mm' to TimePoint.
     * @param       time    'hh:mm' formatted string representing time
     * @return              TimePoint representation
     */
    static TimePoint timeToTimePoint(const std::string& time);

    /**
     * Remove whitespace characters around the string.
     * @param       str     string to make a strip on
     * @return              new string that is original string, but stripped
     */
    static std::string strip(const std::string& str);

    /**
     * Remove newlines from all the string.
     * @param   str     string
     */
    static void removeNewlines(std::string& str);

    /**
     * Replace all occurrences of string 'toReplace' in
     * string 'str' with string 'with'.
     * @param str           string to make replaces on
     * @param toReplace     string to find
     * @param with          string to replace
     */
    static void replaceCharacter(std::string& str, const std::string& toReplace, const std::string& with);


    /**
     * Translator.
     * @tparam K    'K' as a 'key'
     * @tparam V    'V' as a 'value'
     */
    template<typename K, typename V>
    struct Translator {
    public:
        /**
         * Translate from key to value
         * @param   key   key
         * @return        value
         */
        size_t id(const std::string& key) {
            if (keyToId_.find(key) != keyToId_.end()) {
                return keyToId_[key];
            }

            return -1;
        }

        /**
         * Translate from id to value.
         * @param       id  id
         * @return          value
         */
        std::string key(size_t id) {
            return idToKey_[id];
        }

        /**
         * Add new key, return it's new ID
         * @param       key     new key to be inserted
         * @return              it's new ID
         */
        size_t add(std::string& key) {
            if (keyToId_.find(key) != keyToId_.end()) {
                return id(key);
            }

            size_t newId = keyToId_.size();
            keyToId_[key] = newId;
            idToKey_[newId] = key;

            return newId;
        }

        /**
         * For printing.
         * @param os
         * @param translator
         * @return
         */
        friend std::ostream& operator<<(std::ostream& os, Translator<K, V>& translator) {
            for (auto& pair : translator.keyToId_) {
                os << pair.first << " <-> " << pair.second << std::endl;
            }

            return os;
        }
    private:
        /**
         * Key to ID mapping
         */
        std::map<std::string, size_t> keyToId_;

        /**
         * ID to key mapping
         */
        std::map<size_t, std::string> idToKey_;
    };
};


#endif //SCHEDYL_GENERALUTIL_H
