//
// Created by simon on 08.02.22.
//

#include "CallSAT.h"
#include "ProgCall.h"

#include <cstdlib>
#include <fstream>
#include <vector>
#include <sstream>

using namespace std;

bool CallSAT::callSAT(const std::string& cnfFilename, std::vector<int>& solution) {
    // check file exists
    fstream f(cnfFilename);
    if (!f.good()) {
        cerr << "File \"" << cnfFilename << "\" does not exist." << endl;
        exit(1);
    }

    // run SAT solver
    string command = "java -jar vendor/sat4j/org.sat4j.core.jar " + cnfFilename + " | grep -Po \"(^v[[:space:]])\\K.*(?= 0)\"";
    string output = ProgCall::exec(command.c_str());

    if (!output.empty()) {
        stringstream sout(output);
        string variable;
        while (sout >> variable) {
            solution.push_back(stoi(variable));
        }
    }

    // if 'output' is empty, then the formula is unsolvable, otherwise there is solution
    return !output.empty();
}
