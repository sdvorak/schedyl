//
// Created by simon on 08.02.22.
//

#ifndef SCHEDYL_CALLSAT_H
#define SCHEDYL_CALLSAT_H

#include <iostream>
#include <vector>

class CallSAT {
public:
    /**
     * Wrapper for calling Sat4j.
     * @param cnfFilename   file path with file name of .cnf file to run SAT on
     * @param solution      satisfying variable assignment
     * @return              true, if the formula in .cnf file is satisfiable, false otherwise
     */
    static bool callSAT(const std::string& cnfFilename, std::vector<int>& solution);
};


#endif //SCHEDYL_CALLSAT_H
