//
// Created by simon on 08.02.22.
//

#ifndef SCHEDYL_PROGCALL_H
#define SCHEDYL_PROGCALL_H

#include <iostream>

class ProgCall {
public:
    /**
     * Source: https://stackoverflow.com/questions/478898/how-do-i-execute-a-command-and-get-the-output-of-the-command-within-c-using-po
     * @param   cmd     Command to run as in Linux terminal
     * @return          command output
     */
    static std::string exec(const char* cmd);
};


#endif //SCHEDYL_PROGCALL_H
