//
// Created by simon on 17.11.21.
//

#ifndef SCHEDYL_MYSTRUCTURES_H
#define SCHEDYL_MYSTRUCTURES_H

#include <iostream>
#include <iomanip>
#include <map>
#include <vector>
#include <algorithm>

enum DayId { MON = 1, TUE, WED, THU, FRI, SAT, SUN };

inline std::ostream& operator<<(std::ostream& os, DayId di) {
    switch(di) {
        case MON:
            os << "monday";
            break;
        case TUE:
            os << "tuesday";
            break;
        case WED:
            os << "wednesday";
            break;
        case THU:
            os << "thursday";
            break;
        case FRI:
            os << "friday";
            break;
        case SAT:
            os << "saturday";
            break;
        case SUN:
            os << "sunday";
            break;
    }

    return os;
}

/**
 * Class for representing integer in integral interval {min, ..., max},
 * specified by template parameters.
 * @tparam min  lower bound of integral interval
 * @tparam max  upper bound of integral interval
 */
template<int min, int max>
class RangedInteger {
public:
    RangedInteger(int val) {
        if (val > max || val < min) {
            throw std::out_of_range(
                    "Number " + std::to_string(val) + " is not in range from \"" + std::to_string(min) + "\" to \"" +
                    std::to_string(max) + "\".");
        }
        m_val = val;
    }

    RangedInteger operator+(int toAdd) {
        this->m_val += toAdd;
        this->m_val %= max;
        return *this;
    }

    int value() const {
        return m_val;
    }
private:
    int m_val;
};

const int MIN_TIMEID = 1;     // 00:00 has ID=1
const int MAX_TIMEID = 1440;  // 24:59 has ID=1440

typedef RangedInteger<MIN_TIMEID, MAX_TIMEID> TimePoint;

struct TimeRange {
    TimeRange(DayId dayId_, TimePoint timeFrom_, TimePoint timeTo_):
        dayId(dayId_), timeFrom(timeFrom_), timeTo(timeTo_) {}

    DayId dayId;
    TimePoint timeFrom;
    TimePoint timeTo;

    bool operator==(const TimeRange &tr2) {
        return timeFrom.value() == tr2.timeFrom.value() && timeTo.value() == tr2.timeTo.value();
    }

    friend std::ostream& operator<<(std::ostream& os, const TimeRange& tr) {
        int hoursFrom = tr.timeFrom.value() / 60;
        int minutesFrom = tr.timeFrom.value() % 60;

        int hoursTo = tr.timeTo.value() / 60;
        int minutesTo = tr.timeTo.value() % 60;

        os << std::setfill('0')
        << std::setw(2) << hoursFrom << ":" << std::setw(2) << minutesFrom << " - "
        << std::setw(2) << hoursTo << ":" << std::setw(2) << minutesTo
        << " on " << tr.dayId;
        return os;
    }
};

typedef size_t event_id;
typedef size_t sch_subj_id;

struct Event {
    Event(event_id eventId_, sch_subj_id schedulingSubjectId_, TimeRange duration_, std::map<std::string, size_t> meta_):
        eventId(eventId_), schedulingSubjectId(schedulingSubjectId_), duration(duration_), meta(meta_) {}
    /**
     * Unique ID of the scheduling event
     */
    event_id eventId;

    /**
     * Unique ID of the scheduling subject
     */
    sch_subj_id schedulingSubjectId;

    /**
     * Duration of scheduling event
     */
    TimeRange duration;
    /**
     * meta can contain, for example, these keys:
     *    locationId
     *    typeId      (lecture = 1, lab = 2, ...)
     */
    std::map<std::string, size_t> meta;
};


/**
  * EMPTY_GAP                - disables planning of events for specified time interval, e.g. between lunch
  * EMPTY_DAY                - disables planning of events for a specified day (will reuse EMPTY_GAP)
  * LECTURER_PREFERENCE      - specifies preference for specific lecturer (task for MAXSAT weighting)
  * B_LEAST_TRAVELLING       - boolean flag specifying optimisation of minimizing the travelling
  * B_LEAST_TIME_GAPS        - boolean flag specifying optimisation of time gaps between events
  */
enum PREFERENCE_TYPE {EMPTY_DAY, EMPTY_GAP, LECTURER_PREFERENCE, B_LEAST_TRAVELLING, B_LEAST_TIME_GAPS};
// (EMPTY_DAY, DayId), (EMPTY_GAP, TimeRange), (LECTURER_PREFERENCE, scheduling_subject, lecturer_id)

struct UserQuery {
    UserQuery(std::vector<TimeRange> emptyGap_,
              std::vector<DayId> emptyDayId_,
              std::vector<std::pair<int, int>> lecturerPreference_,
              bool leastTravelling_,
              bool leastTimeGaps_):
              emptyGap(emptyGap_),
              emptyDayId(emptyDayId_),
              lecturerPreference(lecturerPreference_),
              leastTravelling(leastTravelling_),
              leastTimeGaps(leastTimeGaps_) {}

    std::vector<TimeRange> emptyGap;
    std::vector<DayId> emptyDayId;
    std::vector<std::pair<int, int>> lecturerPreference;
    bool leastTravelling;
    bool leastTimeGaps;
};



#endif //SCHEDYL_MYSTRUCTURES_H
