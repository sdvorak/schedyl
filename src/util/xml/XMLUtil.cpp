//
// Created by simon on 11.12.21.
//

#include "XMLUtil.h"

std::string XMLUtil::cleanHTML(const std::string &html) {
    // Initialize a Tidy document
    TidyDoc tidyDoc = tidyCreate();
    TidyBuffer tidyOutputBuffer = {0};

    // Configure Tidy
    // The flags tell Tidy to output XML and disable showing warnings
    bool configSuccess = tidyOptSetBool(tidyDoc, TidyXmlOut, yes)
                         && tidyOptSetBool(tidyDoc, TidyQuiet, yes)
                         && tidyOptSetBool(tidyDoc, TidyNumEntities, yes)
                         && tidyOptSetBool(tidyDoc, TidyShowWarnings, no);

    int tidyResponseCode = -1;

    // Parse input
    if (configSuccess) {
        tidyResponseCode = tidyParseString(tidyDoc, html.c_str());
    }

    // Process HTML
    if (tidyResponseCode >= 0) {
        tidyResponseCode = tidyCleanAndRepair(tidyDoc);
    }

    // Output the HTML to our buffer
    if (tidyResponseCode >= 0) {
        tidyResponseCode = tidySaveBuffer(tidyDoc, &tidyOutputBuffer);
    }

    // Any errors from Tidy?
    if (tidyResponseCode < 0) {
        throw std::runtime_error("Tidy encountered an error while parsing an HTML response. Tidy response code: " + std::to_string(tidyResponseCode));
    }

    // Grab the result from the buffer and then free Tidy's memory
    std::string tidyResult = (char*)tidyOutputBuffer.bp;
    tidyBufFree(&tidyOutputBuffer);
    tidyRelease(tidyDoc);

    return tidyResult;
}

std::string XMLUtil::getTextByXPath(const pugi::xpath_node &node, const std::string &xpath) {
    const pugi::xpath_node found_node = node.node().select_node(xpath.c_str());
    std::string node_txt_content = found_node.node().text().get();
    return node_txt_content;
}

std::string XMLUtil::getTextByXPath(const pugi::xml_document &doc, const std::string &xpath) {
    const pugi::xpath_node found_node = doc.select_node(xpath.c_str());
    std::string node_txt_content = found_node.node().text().get();
    return node_txt_content;
}
