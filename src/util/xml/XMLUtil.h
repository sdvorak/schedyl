//
// Created by simon on 11.12.21.
//

#ifndef SCHEDYL_XMLUTIL_H
#define SCHEDYL_XMLUTIL_H

#include "tidy.h"
#include "tidybuffio.h"

#include "pugixml.hpp"

class XMLUtil {
public:
    /**
     * Wrapper for searching textual content inside XML element specified by 'xpath' in document.
     * @param doc       XML document
     * @param xpath     XPath to XML element
     * @return          found textual content
     */
    static std::string getTextByXPath(const pugi::xml_document& doc, const std::string& xpath);

    /**
     * Wrapper for searching textual content inside XML element specified by 'xpath' in node.
     * @param node      XML node
     * @param xpath     XPath to XML element
     * @return          found textual content
     */
    static std::string getTextByXPath(const pugi::xpath_node& node, const std::string& xpath);

    /**
     * Clean HTML, that is not well formatted using 'HTML Tidy' library.
     * @param   html    HTML as a string
     * @return          well-formatted HTML
     */
    static std::string cleanHTML(const std::string &html);
};


#endif //SCHEDYL_XMLUTIL_H
