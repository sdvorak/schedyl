//
// Created by simon on 17.11.21.
//

#include "gtest/gtest.h"
#include "../src/bl/io/eventdata/JsonEventTransformer.h"
#include <string>

using namespace std;

// Test valid JSON Schema
TEST(JsonEventDataTest, TestJSONValidation01) {
    string filename = "test/data/eventdata/in0-anon.json";
    JsonEventTransformer jet;
    jet.getData(filename);

    EXPECT_TRUE(jet.validate());
}

TEST(JsonEventDataTest, TestJSONValidation02) {
    string filename = "test/data/eventdata/in1-INVALID.json";
    JsonEventTransformer jet;
    jet.getData(filename);

    EXPECT_FALSE(jet.validate());
}

TEST(JsonEventDataTest, TestJSONValidation03) {
    string filename = "test/data/eventdata/in2-INVALID.json";
    JsonEventTransformer jet;
    jet.getData(filename);

    EXPECT_FALSE(jet.validate());
}

TEST(JsonEventDataTest, TestJSONValidation04) {
    string filename = "test/data/eventdata/in3-INVALID.json";
    JsonEventTransformer jet;
    jet.getData(filename);

    try {
        jet.validate();
    } catch (runtime_error& e) {
        EXPECT_TRUE(true);
    }
}

TEST(JsonEventDataTest, TestJSONValidation05) {
    string filename = "test/data/eventdata/in4-EMPTY.json";
    JsonEventTransformer jet;
    jet.getData(filename);

    EXPECT_TRUE(jet.validate());
}

TEST(JsonEventDataTest, TestJSONValidation06) {
    string filename = "test/data/eventdata/in5-INVALID.json";
    JsonEventTransformer jet;
    jet.getData(filename);

    EXPECT_FALSE(jet.validate());
}
