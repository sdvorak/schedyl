in0-anon.json - valid json

in1-INVALID.json - json is not according to schema because (first) TimeRange contains just two integers.

in2-INVALID.json - json is invalid because it contains non-string value (``"thisIsNotAllowedBecauseThereIsInteger": 5``)

in3-INVALID.json - invalid json (structure)

in5-INVALID.json - TIME_TO is 1600, which is not in range 1 to 1440



